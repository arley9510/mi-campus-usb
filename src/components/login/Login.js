import React, {Component} from 'react';
import {
    TextInput,
    Button,
    Text,
    View,
} from 'react-native';

import AsyncStorageService from "../../services/AsyncStorageService";
import NavigationService from "../../services/NavigationService";
import ActionLogin from './ActionLogin';

export default class Login extends Component<{}> {

    state = {
        email: '',
        password: '',
    };

    constructor(props) {
        super(props);

        AsyncStorageService.validateLogin().then((response) => {
            if (response !== null){
                NavigationService.navigate('Home');
            }
        });
    }

    login = async () => {
        const actionLogin = new ActionLogin();
        actionLogin._userLogin(this.state);
    };

    render() {
        return (
            <View>
                <Text>
                    Email
                </Text>
                <TextInput
                    onChangeText={(email) => this.setState({email})}
                    keyboardType='email-address'
                    autoCapitalize='none'
                    autoFocus={true}
                />
                <Text>
                    Password
                </Text>
                <TextInput
                    onChangeText={(password) => this.setState({password})}
                    secureTextEntry={true}
                />

                <Button
                    onPress={this.login}
                    disabled={this.state.email.length <= 0 || this.state.password.length <= 0}
                    title='Log in'
                />
            </View>
        );
    }
}