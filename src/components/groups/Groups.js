import React, {Component} from 'react';
import {View, Text, Alert} from 'react-native';

import AsyncStorageService from './../../services/AsyncStorageService';

export default class Groups extends Component {

    state = {token: ''};

    constructor(props) {
        super(props);
        AsyncStorageService.getItem('token')
            .then((response) => {
                                this.setState({token: response});
            });
    }

    render() {
        return (
            <View>
                <Text>
                    {this.state.token}
                </Text>
            </View>
        );
    }
}