import {StackNavigator} from 'react-navigation';
import React, {Component} from 'react';

import NavigationService from "./services/NavigationService";
import Secure from "./components/security/Secure";
import Login from "./components/login/Login";
import Home from "./components/home/Home";

export default class Index extends Component {

    render() {
        return <AppNavigator
            ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
            }}
        />
    }
}

export const AppNavigator = StackNavigator({
    Secure: {screen: Secure, navigationOptions: {title: 'Mi campus-USB'}},
    Login: {screen: Login, navigationOptions: {title: 'Mi campus-USB'}},
    Home: {screen: Home, navigationOptions: {title: 'Mi campus-USB', headerLeft: null}},
});
