import React, {Component} from 'react';
import {WebView} from 'react-native';

import Environment from "../../enviroments/enviroment";

export default class News extends Component {
    render() {
        return (
            <WebView
                source={{uri: Environment.USB_TWITTER}}
                javaScriptEnabled={true}
            />
        );
    }
}