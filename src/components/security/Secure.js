import React, {Component} from 'react';

import AsyncStorageService from './../../services/AsyncStorageService'
import ActivityLoad from "../indicator/ActivityLoad";
import Login from "../login/Login";
import Home from "../home/Home";

export default class Secure extends Component {

    state = {
        loggedIn: null
    };

    constructor(props) {
        super(props);
        AsyncStorageService.validateLogin().then((response) => {
            if (response !== null) {
                this.setState({
                    loggedIn: true
                });
            } else {
                this.setState({
                    loggedIn: false
                });
            }
        });
    }

    render() {

        if (this.state.loggedIn === null) {
            return <ActivityLoad/>
        } else if (this.state.loggedIn === true) {
            return <Home/>
        } else {
            return <Login/>
        }
    }
}