import {TabNavigator} from 'react-navigation';

import Friend from "../friends/Friend";
import Groups from "../groups/Groups";
import News from "../news/News";

const TabNav = TabNavigator({
    News: {screen: News},
    Groups: {screen: Groups},
    Friends: {screen: Friend},
});

export default (TabNav);