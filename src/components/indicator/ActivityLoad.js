import React, {Component} from 'react';
import {
    ActivityIndicator,
    StyleSheet,
    View,
} from 'react-native';

export default class ActivityLoad extends Component {
    static navigationOptions = {
        header: { visible: false }
    };

    render() {
        return (
            <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#FF9800"/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    }
});