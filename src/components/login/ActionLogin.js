import Environment from '../../enviroments/enviroment';
import React, {Component} from 'react';
import {Alert} from 'react-native';

import AsyncStorageService from './../../services/AsyncStorageService';
import NavigationService from "../../services/NavigationService";

export default class ActionLogin extends Component {

    async _userLogin(state) {

        let DeviceInfo = require('react-native-device-info');
        const id = DeviceInfo.getUniqueID();

        return await fetch(Environment.CLIENT_API + 'login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: state.email,
                password: state.password,
                device: id,
                grant_type: 'password'
            })
        }).then((response) => {

            if (response.status === 201) {

                return response.json();
            } else {

                Alert.alert('Error', 'Email o Contraseña incorrectos');
            }

        }).then((responseJson) => {

            if (typeof responseJson !== 'undefined') {
                const token = responseJson['token'];

                AsyncStorageService.setItem('token', token).then();
                NavigationService.navigate('Home');
            }

        }).catch(err => {
            Alert.alert(err.toString());
        });
    }
}
