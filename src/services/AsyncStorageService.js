import {AsyncStorage, Alert} from 'react-native';

async function setItem(key, value) {
    try {
        AsyncStorage.setItem(key, value);
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

async function getItem(key) {

    try {
        return await AsyncStorage.getItem(key);
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }

}

async function clearItem() {
    try {
        AsyncStorage.clear();
    } catch (e) {
        Alert.alert('Error', 'Se produjo un error inesperado');
    }
}

async function validateLogin() {

    return await AsyncStorage.getItem('token');
}

export default {
    validateLogin,
    clearItem,
    setItem,
    getItem,
};