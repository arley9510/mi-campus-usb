import React, {Component} from 'react';
import {View, Text, Button, Alert} from 'react-native';

import AsyncStorageService from "../../services/AsyncStorageService";
import NavigationService from "../../services/NavigationService";
import Environment from "../../enviroments/enviroment";

export default class Friend extends Component {

    async logout() {
        let id;

        let DeviceInfo = require('react-native-device-info');
        id = DeviceInfo.getUniqueID();

        await AsyncStorageService.getItem('token').then((token) => {
            return fetch(Environment.CLIENT_API + 'logout', {
                method: 'PATCH',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    'token': token,
                    'device': id,
                })
            }).then((response) => {

                if (response.status === 204) {

                    AsyncStorageService.clearItem().then();
                    NavigationService.resetRoute();
                } else {
                    Alert.alert('Error', 'Intentalo más tarde');
                }

            }).catch(err => {
                Alert.alert(err.toString());
            });
        }).catch(err => {
            Alert.alert(err.toString());
        });
    }

    render() {
        return (
            <View>
                <Text>
                    Friends
                </Text>
                <Button
                    onPress={this.logout}
                    title='Log Out'
                />
            </View>
        );
    }
}

