import React, {Component} from 'react';

import AsyncStorageService from "../../services/AsyncStorageService";
import NavigationService from "../../services/NavigationService";
import TabNav from './../tabnavigation/TabNav'

export default class Home extends Component {

    constructor(props) {
        super(props);

        AsyncStorageService.validateLogin().then((response) => {
            if (response === null){
                NavigationService.resetRoute();
            }
        });
    }

    render() {
        return <TabNav/>;
    }
}